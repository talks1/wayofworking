# Way of Working

## Current 

Our current team methodology (6D) seems to be to do work in the following stages

- Discover
- Define
- Design
- Delivery
- Deploy
- Drive

# Hypothesis

The 'Current' way of working will 
- not efficiently adapt to the arrival of new information...particularly that which surfaces in deliver and drive
- will deliver artefacts which do not produce value to the customer
- require value-less scoping and estimation


In essence it will not be 'continuous' enough.

## Evidence Supporting Hypothesis

- Ground Gauge Phase 2.5 delivery process - developed solution which doesnt align well to current business processes 
- Production Transition effort - there is significant activity but little verification of values.

# Questions

What other 'ways of working' options were considered?

The current 'way of working' appears similar to [Disciplined Agile](https://www.pmi.org/disciplined-agile/process/introduction-to-dad)

Possibly we could realize 6D through a [Spiral Model](https://en.wikipedia.org/wiki/Spiral_model) of software delivery.  And in the limit that the iterations were sufficiently short could be considered continuous.

# Suggested Option

In place of 6D the suggestion is for a 4C methodology

- Continuous Communication, Elicitation, Collation and Consideration
- Continuous Task Out and Prioritization
- Continuous Test, Develop and Delivery
- Continuous Measure, Monitor

This is partly based on [Cynefin Framework](https://medium.com/@warren2lynch/scrum-what-is-cynefin-framework-8c33c01bcc19) and view that a creative technology team is going to be 
- operating in a *Complex* environment where it is appropriate to probe then sense then respond
- and not a *Simple* environment where one normally senses , categories and then respond 
- nor a *Complicated* environment where one senses - analyzes - responds.
